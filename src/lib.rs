use sampled_data_duration::ConstantRateDuration;
use std::collections::HashSet;
use std::fmt;

const MAX_24_BIT_SAMPLE_VALUE_AS_I32: i32 = 8388608;
const MIN_24_BIT_SAMPLE_VALUE_AS_I32: i32 = -8388607;

fn dbfs(value: i32, full_scale_value: i32) -> f64 {
    20.0 * (value as f64 / full_scale_value as f64).log10()
}

fn bits_required(values: usize) -> f64 {
    (values as f64).log2()
}

pub struct ChannelStats {
    max: i32,
    max_index: u64,
    min: i32,
    min_index: u64,
    abs_max: i32,
    abs_max_index: u64,
    abs_min: i32,
    abs_min_index: u64,
    rate: u64,
    values_set: HashSet<i32>,
}
impl ChannelStats {
    pub fn new(rate: u64) -> ChannelStats {
        ChannelStats {
            max: i32::MIN,
            max_index: u64::MAX,
            min: i32::MAX,
            min_index: u64::MAX,
            abs_max: 0,
            abs_max_index: u64::MAX,
            abs_min: i32::MAX,
            abs_min_index: u64::MAX,
            rate: rate,
            values_set: HashSet::new(),
        }
    }

    pub fn update(&mut self, sample: i32, index: u64) {
        if sample > self.max {
            self.max = sample;
            self.max_index = index;
        }

        if sample < self.min {
            self.min = sample;
            self.min_index = index;
        }

        let abs_sample = sample.abs();

        if abs_sample > self.abs_max {
            self.abs_max = abs_sample;
            self.abs_max_index = index;
        }

        if abs_sample < self.abs_min {
            self.abs_min = abs_sample;
            self.abs_min_index = index;
        }

        self.values_set.insert(sample);
    }
}
impl fmt::Display for ChannelStats {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Max = {max} / {fs_max} = {max_dbfs:.2} dBFS @ {max_index} = {max_crd}\n\
             Min = {min} / {fs_min} = {min_dbfs:.2} dBFS @ {min_index} = {min_crd}\n\
             Abs Max = {abs_max} / {fs_max} = {abs_max_dbfs:.2} dBFS @ {abs_max_index} = {abs_max_crd}\n\
             Abs Min = {abs_min} / {fs_max} = {abs_min_dbfs:.2} dBFS @ {abs_min_index} = {abs_min_crd}\n\
             Values = {values} = {values_bits:.2} bits\n\
             One sample dBFS = {one_sample:.2} dBFS",
            max = self.max,
            max_dbfs = dbfs(self.max, MAX_24_BIT_SAMPLE_VALUE_AS_I32),
            max_index = self.max_index,
            max_crd = ConstantRateDuration::new(self.max_index, self.rate),
            min = self.min,
            min_dbfs = dbfs(self.min, MIN_24_BIT_SAMPLE_VALUE_AS_I32),
            min_index = self.min_index,
            min_crd = ConstantRateDuration::new(self.min_index, self.rate),
            abs_max = self.abs_max,
            abs_max_dbfs = dbfs(self.abs_max, MAX_24_BIT_SAMPLE_VALUE_AS_I32),
            abs_max_index = self.abs_max_index,
            abs_max_crd = ConstantRateDuration::new(self.abs_max_index, self.rate),
            abs_min = self.abs_min,
            abs_min_dbfs = dbfs(self.abs_min, MAX_24_BIT_SAMPLE_VALUE_AS_I32),
            abs_min_index = self.abs_min_index,
            abs_min_crd = ConstantRateDuration::new(self.abs_min_index, self.rate),
            values = self.values_set.len(),
            values_bits = bits_required(self.values_set.len()),
            fs_max = MAX_24_BIT_SAMPLE_VALUE_AS_I32,
            fs_min = MIN_24_BIT_SAMPLE_VALUE_AS_I32,
            one_sample = dbfs(1, MAX_24_BIT_SAMPLE_VALUE_AS_I32),
        )
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
