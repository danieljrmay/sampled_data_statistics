use claxon::metadata::StreamInfo;
use claxon::FlacReader;
use sampled_data_statistics::ChannelStats;
use std::fmt;

pub fn main() {
    println!("flac-stats");

    let mut reader = FlacReader::open("test.flac").unwrap();
    let streaminfo = reader.streaminfo();

    let mut left_chst = ChannelStats::new(streaminfo.sample_rate as u64);
    let mut right_chst = ChannelStats::new(streaminfo.sample_rate as u64);

    let mut samples_iter = reader.samples();
    let mut continue_to_read: bool = true;
    let mut index: u64 = 0;

    while continue_to_read {
        if let Some(Ok(left_sample)) = samples_iter.next() {
            left_chst.update(left_sample, index);
        } else {
            continue_to_read = false;
        }

        if let Some(Ok(right_sample)) = samples_iter.next() {
            right_chst.update(right_sample, index);
        } else {
            continue_to_read = false;
        }

        index += 1;
    }

    println!("Stream info = {:?}", streaminfo);
    println!("Bits per sample = {}", streaminfo.bits_per_sample);
    println!("Channels = {}", streaminfo.channels);
    println!("Sampling rate = {}", streaminfo.sample_rate);
    println!("Samples per channel = {}", streaminfo.samples.unwrap());

    println!("\nLeft channel:\n{}", left_chst);
    println!("\nRight channel:\n{}", right_chst);
}
